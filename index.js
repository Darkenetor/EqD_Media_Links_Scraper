"use strict"

const fetch = require('node-fetch')
const {JSDOM} = require('jsdom')
const fs = require('fs-extra')
const IO = {
		getDataSync : srcPath => {
			try {
				return fs.readFileSync( srcPath, 'utf8', err => {
					if ( err ) throw Error( err )
				})
			} catch (e) {
				return null
			}
		},
		writeData : (dstPath, d, cb) =>
				fs.writeFileSync( dstPath, d, err => {
					if ( err ) throw Error( err )
					return ( cb ) ? cb(d) : undefined
				}),
	}
const log = (...args) =>
	console.log( ...args )
const die = (msg, data) => {
	IO.writeData( './err/document.html', data )
	IO.writeData( './err/err.txt', data )
	throw Error( msg )
}
const delay = (ms, val) =>
	new Promise( resolve =>
   		setTimeout(
   			() => resolve( val ),
   			ms
   		)
   	)
Object.getOwnPropertyNames(Array.prototype).forEach( m =>
	(! Object.prototype[m] ) ? (
		Object.prototype[m] = Array.prototype[m]
	):null
)

const getPage = async function (URL, raw = false) {
	await delay( (Math.random()*3+0.5)*1000 )

	const d = await ( await fetch(
		URL,
		/*{ credentials: 'include', headers: { 'Cookie': 'view_mature=true' } }*/
	) ).text()

	return raw ? d : new JSDOM( d ).window
}

const tags = [
		//"Drawfriend",
		//"Drawfriend Human Anthro Pony",
		//"Music of the Day",
		//"Bonus Music",
	]

let indexes, lastUpdate, threads, threadNames, links, linksDiscarded, lastTag, lastProcessed

;( async () => { try {

	log( ' STARTING' )

	for ( let tag of tags ) {

		lastTag = tag
		log( `- Working on: ${tag}` )

		lastUpdate = ( IO.getDataSync( `./data/${tag}/lastUpdate.txt` ) ||'').trim() || '0'
		indexes = JSON.parse( IO.getDataSync( `./data/${tag}/indexes.json` ) || `["https://www.equestriadaily.com/search/label/${tag.replace( / /g, '%20' )}?updated-min=${lastUpdate.replace( /\d+(?=-\d+:\d+$)/, '01' )}&updated-max=${(new Date()).toISOString().replace( /\..+(?=Z$)/, '' )}&max-results=50&start=0&by-date=false&m=1"]` )
		threads = []
		threadNames = []
		JSON.parse( IO.getDataSync( `./data/${tag}/threads.json` ) || '[]').forEach( e => {
			threadNames.push( e[0] )
			threads.push( e[1] )
		})
		links = JSON.parse( IO.getDataSync( `./data/${tag}/links.json` ) || '[]' )
		linksDiscarded = JSON.parse( IO.getDataSync( `./data/${tag}/linksDiscarded.json` ) || '[]' )

		log( '-- Indexing threads now.' )

		// const threads = []
		// const threadNames = []
		for ( let URL of indexes ) {
			log( `--- Scraping: ${URL}` )

			const {document} = await getPage( URL )

			// const links = [...document.querySelectorAll( '.jump-link a[href]' )]	// desktop version
			const links = [...document.querySelectorAll( '.post-title a[href]' )]

			log( `---- Threads found: ${links.length}` )

			if ( !links.length )
				if ( '\nNo posts with label ' === document.querySelector( '.status-msg-body' ).childNodes[0].nodeValue )
					break;
				else
					die( URL, document.documentElement.outerHTML )

			IO.writeData( `./data/${tag}/indexes/${ URL
				.match( /\/([^\/]*)$/i )[1]
				.replace( /[<>:"/\\|?*]/g, '_' )
			}.html`, document.documentElement.outerHTML )

			links.forEach( e => {
				threads.push( e.href )
				threadNames.push( document.title.replace( /^.*?: /i, '' ) )
			})

			indexes.push(
				document.querySelector('[class="blog-pager-older-link"]').href
			)

			lastProcessed = document.querySelector( '.timestamp-link abbr[title]' ).title
		}
		IO.writeData( `./data/${tag}/indexes.json`, JSON.stringify( indexes, undefined, '\t' ) )
		const zipThreads = threadNames.map( (e, i) => [e, threads[i]] )
		IO.writeData( `./data/${tag}/threads.json`, JSON.stringify( zipThreads, undefined, '\t' ) )
		IO.writeData( `./data/${tag}/lastUpdate.txt`, lastProcessed )

		log( '-- DONE Indexing Threads! Scraping links now.' )

		// const links = []
		for ( let URL of threads ) {
			log( `--- Scraping: ${URL}` )

			const {document} = await getPage( URL )

			//document.querySelectorAll('a[imageanchor][href*="youtube.com"]')
			/*document.querySelectorAll('.post-body b a')
				.filter( e =>
					/\[\d+\] source/i
					.test( e.innerText.trim() )
				)*/
			const pbody = document.querySelector( '.post-body' )
			pbody.innerHTML = pbody.innerHTML.replace( /^.*\n<a name="more"><\/a>\n/i, '' )

			const sourcesRaw = document.querySelectorAll( '.post-body a[href]:not([imageanchor])' )
				.map( e => e.href )
			log( `---- Links found: ${sourcesRaw.length}` )

			const sourcesGood = []
			const sourcesDiscarded = []
			sourcesRaw.forEach( s =>
				[
					/youtube\.com\/watch\?v=\w+/i,
					/youtu.be\/\w+\/?/i,
					/soundcloud\.com\/[\w\-]+\/[\w\-]+\/?/i,
					/[\w\-]+\.deviantart\.com\/art\/[\w\-]+-\d+/i,
					/fav\.me\/\w+/i,
					/[\w\-]+\.tumblr\.com\/post\/\d+\/?(\/[\w\-]+\/?)?/i,
					/derpibooru\.org\/\d+\/?/i,
					/pixiv\.net\/member_illust\.php\?(\w+=\w+&)*illust_id=\d+/i,
					/\w+\.googleusercontent\.com\/-(\w+\/)+\w+\.\w+\/?\??/i,
				].some( r => r.test(s) )
					? sourcesGood.push(s)
					: sourcesDiscarded.push(s)
			)
			log( `---- Of which invalid: ${sourcesDiscarded.length}` )

			if ( !sourcesGood.length )
				die( URL, document.documentElement.outerHTML )

			IO.writeData( `./data/${tag}/threads/${ URL
				.match( /(\d+\/\d+\/[^/]+)\.html/i )[1]
				.replace( /[<>:"/\\|?*]/g, '_' )
			}.html`, document.documentElement.outerHTML )

			links.push( sourcesGood )
			linksDiscarded.push( sourcesDiscarded )
		}
		IO.writeData( `./data/${tag}/links.json`, JSON.stringify( links, undefined, '\t' ) )
		IO.writeData( `./data/${tag}/linksDiscarded.json`, JSON.stringify( linksDiscarded, undefined, '\t' ) )

		log( '-- DONE Scraping Threads! Saving Links now.' )

		const listLinks = links
			.map( e => e
				.join( '\n' )
			).join( '\n' )
		IO.writeData( `./data/${tag}/links.txt`, listLinks )

	}

	log( ' DONE' )

} catch (e) {
	console.log( e.stack )

	IO.writeData( './err/lastTag.txt', lastTag )
	IO.writeData( './err/indexes.json', JSON.stringify( indexes, undefined, '\t' ) )
	IO.writeData( `./err/lastProcessed.txt`, lastProcessed )
	const zipThreads = threadNames.map( (e, i) => [e, threads[i]] )
	IO.writeData( './err/threads.json', JSON.stringify( zipThreads, undefined, '\t' ) )
	IO.writeData( './err/links.json', JSON.stringify( links, undefined, '\t' ) )

	process.exit(1)
} })()
